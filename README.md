# Sistema Inoperativo

Questo è il repository della campagna "Sistema Inoperativo".

https://sistemainoperativo.it/

## Aggiornare di produzione

Il sito online è aggiornato automaticamente dalle ultime modifiche ogni 3 ore.

Il sito è in produzione sul server `servizi.linux.it`.

## Licenza

La licenza dei contenuti è la CC BY, Attribuzione, da Italian Linux Society e contributori.

https://creativecommons.org/licenses/by-sa/4.0/deed.it

Per i nomi di tutti i contributori, vedi lo storico di git.
